variable "prometheus_namespace" {
  type        = string
  description = "Prometheus namespace."
}

variable "prometheus_chart_version" {
  type        = string
  description = "Prometheus Helm version."
  default     = "11.12.1"
}