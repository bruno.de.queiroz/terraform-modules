terraform {
  required_providers {
    helm = {
      source  = "hashicorp/helm"
      version = ">= 2.3.0"
    }
  }
}

resource "helm_release" "prometheus" {
  name       = "prometheus"
  chart      = "prometheus"
  repository = "https://charts.helm.sh/stable"
  version    = var.prometheus_chart_version
  namespace  = var.prometheus_namespace

  values = [
    file("${path.module}/values.yaml")
  ]
}