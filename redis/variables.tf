variable "cluster_name" {
  description = "Cluster name"
  type        = string
}

variable "cluster_region" {
  description = "Region ID"
  type        = string
  default     = "fra1"
}

variable "cluster_size" {
  description = "Cluster size"
  type        = string
  default     = "db-s-1vcpu-1gb"
}

variable "cluster_node_count" {
  description = "Cluster node count"
  type        = number
  default     = 1
}

variable "redis_version" {
  description = "Redis version"
  type        = string
  default     = "6"
}

variable "kubernetes_cluster_id" {
  description = "Kubernetes cluster id"
  type        = string
  default     = "arive-staging-fra1"
}