output "cluster_name" {
  value = digitalocean_database_cluster.redis_cluster.name
}

output "cluster_urn" {
  value = digitalocean_database_cluster.redis_cluster.urn
}

output "cluster_host" {
  value = digitalocean_database_cluster.redis_cluster.host
}

output "cluster_private_host" {
  value = digitalocean_database_cluster.redis_cluster.private_host
}

output "cluster_port" {
  value = digitalocean_database_cluster.redis_cluster.port
}

output "cluster_uri" {
  value = digitalocean_database_cluster.redis_cluster.uri
}

output "cluster_private_uri" {
  value = digitalocean_database_cluster.redis_cluster.private_uri
}

output "cluster_database" {
  value = digitalocean_database_cluster.redis_cluster.database
}

output "cluster_user" {
  value = digitalocean_database_cluster.redis_cluster.user
}

output "cluster_password" {
  sensitive = true
  value     = digitalocean_database_cluster.redis_cluster.password
}