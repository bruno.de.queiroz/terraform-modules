terraform {
  required_providers {
    digitalocean = {
      source  = "digitalocean/digitalocean"
      version = "~> 2.0"
    }
  }
}

resource "digitalocean_database_cluster" "redis_cluster" {
  name            = "${var.cluster_name}-${var.cluster_region}"
  region          = var.cluster_region
  engine          = "redis"
  version         = var.redis_version
  size            = var.cluster_size
  node_count      = var.cluster_node_count
  eviction_policy = "volatile_ttl"
}

resource "digitalocean_database_firewall" "redis_firewall" {
  cluster_id = digitalocean_database_cluster.redis_cluster.id

  rule {
    type  = "k8s"
    value = var.kubernetes_cluster_id
  }
}