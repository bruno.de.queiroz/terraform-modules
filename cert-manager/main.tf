terraform {
  required_providers {
    kubectl = {
      source  = "gavinbunney/kubectl"
      version = ">= 1.11.3"
    }
    helm = {
      source  = "hashicorp/helm"
      version = ">= 2.3.0"
    }
  }
}

resource "helm_release" "cert_manager" {
  name       = "cert-manager"
  chart      = "cert-manager"
  repository = "https://charts.jetstack.io"
  version    = var.cert_manager_chart_version
  namespace  = var.cert_manager_namespace

  values = [
    file("${path.module}/values.yaml")
  ]
}

data "kubectl_path_documents" "cert_manager_secrets" {
  pattern = "${path.module}/manifests/secrets.yaml"
  vars = {
    "namespace"          = var.cert_manager_namespace
    "dns_provider_token" = base64encode("${var.dns_provider_token}")
  }
}

data "kubectl_path_documents" "cert_manager_issuers" {
  pattern = "${path.module}/manifests/issuers.yaml"
  vars = {
    "namespace"       = var.cert_manager_namespace
    "issuer_email"    = var.cert_manager_issuer_email
    "issuer_acme_url" = var.cert_manager_issuer_acme_url
    "ingress_class"   = var.cert_manager_issuer_ingress_class
  }
}

data "kubectl_path_documents" "cert_manager_certificates" {
  pattern = "${path.module}/manifests/certificates.yaml"
  vars = {
    "tls_certificate_domains" = join(",", var.tls_certificate_domains)
    "namespace"               = var.cert_manager_namespace
    "wildcard_secret_name"    = var.cert_manager_wildcard_secret_name
  }
}

resource "kubectl_manifest" "cert_manager_secrets_manifest" {
  depends_on = [helm_release.cert_manager]
  count      = 1
  yaml_body  = element(data.kubectl_path_documents.cert_manager_secrets.documents, count.index)
}

resource "kubectl_manifest" "cert_manager_issuers_manifest" {
  depends_on = [kubectl_manifest.cert_manager_secrets_manifest]
  count      = 2
  yaml_body  = element(data.kubectl_path_documents.cert_manager_issuers.documents, count.index)
}

resource "kubectl_manifest" "cert_manager_certificates" {
  depends_on = [kubectl_manifest.cert_manager_issuers_manifest]
  count      = 1
  yaml_body  = element(data.kubectl_path_documents.cert_manager_certificates.documents, count.index)
}
