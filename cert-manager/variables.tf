variable "tls_certificate_domains" {
  type        = list(string)
  description = "List of domains to generate the TLS for."
}

variable "dns_provider_token" {
  type        = string
  description = "DNS Provider token."
}

variable "cert_manager_chart_version" {
  type        = string
  description = "Cert Manager Helm version."
  default     = "1.5.3"
}

variable "cert_manager_namespace" {
  type        = string
  description = "Cert Manager namespace."
  default     = "cert-manager"
}

variable "cert_manager_issuer_email" {
  type        = string
  description = "Cert Manager issuer email."
}

variable "cert_manager_issuer_acme_url" {
  type        = string
  description = "Cert Manager issuer acme url."
  default     = "https://acme-v02.api.letsencrypt.org/directory"
}

variable "cert_manager_issuer_ingress_class" {
  type        = string
  description = "Cert Manager issuer ingress class."
}

variable "cert_manager_wildcard_secret_name" {
  type        = string
  description = "Cert Manager wildcard certificate secret name."
  default     = "wildcard-secret"
}
