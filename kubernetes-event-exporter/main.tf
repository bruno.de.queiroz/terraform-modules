
resource "helm_release" "kubernetes-event-exporter" {
  name       = "kubernetes-event-exporter"
  chart      = "kubernetes-event-exporter"
  repository = "https://charts.bitnami.com/bitnami"
  namespace  = "kube-system"
  values = [
    file("${path.module}/values.yaml")
  ]
}