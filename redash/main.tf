terraform {
  required_providers {
    helm = {
      source  = "hashicorp/helm"
      version = ">= 2.3.0"
    }
  }
}

data "template_file" "redash_values" {
  template = file("${path.module}/values.yaml")

  vars = {
    DOMAIN                   = var.domain
    REDASH_COOKIE_SECRET     = var.redash_cookie_secret
    REDASH_SECRET_KEY        = var.redash_secret_key
    REDASH_POSTGRES_PASSWORD = var.redash_postgres_password
    REDASH_REDIS_PASSWORD    = var.redash_redis_password
  }
}

resource "helm_release" "redash" {
  name       = "redash-release"
  repository = "https://getredash.github.io/contrib-helm-chart/"
  chart      = "redash"

  version          = var.redash_chart_version
  namespace        = var.redash_namespace
  create_namespace = true

  values = [
    data.template_file.redash_values.rendered
  ]
}
