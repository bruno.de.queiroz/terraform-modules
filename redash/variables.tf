variable "domain" {
  type        = string
  description = "Default domain."
}

variable "redash_namespace" {
  type        = string
  description = "Redash namespace."
}

variable "redash_chart_version" {
  type        = string
  description = "Redash Helm version."
  default     = "2.3.1"
}

variable "redash_cookie_secret" {
  type        = string
  description = "Redash cooke secret."
  default     = "cookie_secret123"
}

variable "redash_secret_key" {
  type        = string
  description = "Redash secret key."
  default     = "cookie_secret123"
}

variable "redash_postgres_password" {
  type        = string
  description = "Redash Postgres password."
  default     = "dbpassword"
}

variable "redash_redis_password" {
  type        = string
  description = "Redash redis password."
  default     = "dbpassword"
}
