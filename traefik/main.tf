terraform {
  required_providers {
    kubernetes = {
      source  = "hashicorp/kubernetes"
      version = ">= 2.0.0"
    }
    kubectl = {
      source  = "gavinbunney/kubectl"
      version = ">= 1.11.3"
    }
    helm = {
      source  = "hashicorp/helm"
      version = ">= 2.3.0"
    }
  }
}

resource "kubernetes_config_map" "ingress_dynamic_config" {
  metadata {
    name      = "traefik-dynamic-config"
    namespace = var.ingress_gateway_namespace
  }

  data = {
    "dynamic.yaml" = "${file("${path.module}/dynamic.yaml")}"
  }
}

resource "kubernetes_secret" "ingress_dashboard_user" {
  metadata {
    name      = "traefik-dashboard-auth"
    namespace = var.ingress_gateway_namespace
  }

  data = {
    users = var.ingress_gateway_dashboard_credentials
  }
}

resource "helm_release" "ingress_gateway" {
  depends_on = [kubernetes_config_map.ingress_dynamic_config]
  name       = "traefik"
  chart      = "traefik"
  repository = "https://helm.traefik.io/traefik"
  version    = var.ingress_gateway_chart_version
  namespace  = var.ingress_gateway_namespace
  values = [
    templatefile("${path.module}/values.yaml", {
      apm_server_url          = var.apm_server_url,
      apm_secret_token        = var.apm_secret_token
      apm_env                 = var.apm_env
      traefik_requests_cpu    = var.traefik_requests_cpu
      traefik_requests_memory = var.traefik_requests_memory
    })
  ]
}

data "kubectl_path_documents" "ingress_tls_store" {
  pattern = "${path.module}/manifests/tls_store.yaml"
  vars = {
    "namespace"            = var.ingress_gateway_namespace
    "wildcard_secret_name" = var.ingress_gateway_wildcard_secret_name
  }
}

data "kubectl_path_documents" "ingress_middlewares" {
  pattern = "${path.module}/manifests/middlewares.yaml"
  vars = {
    "namespace"     = var.ingress_gateway_namespace
    "erp_key"       = var.erp_key
    "erp_namespace" = var.erp_namespace
  }
}

data "kubectl_path_documents" "ingress_routes" {
  pattern = "${path.module}/manifests/routes.yaml"
  vars = {
    "domain"        = var.domain
    "namespace"     = var.ingress_gateway_namespace
    "eck_namespace" = var.eck_namespace,
    "eck_kibana"    = var.eck_kibana
    "erp_url"       = var.erp_url
    "erp_namespace" = var.erp_namespace
    "erp_service"   = var.erp_service
  }
}

data "kubectl_path_documents" "otp_routes" {
  pattern = "${path.module}/manifests/otp_routes.yaml"
  vars = {
    "domain"                = var.otp_domain
    "blocked_x_uid_headers" = var.blocked_x_uid_headers
  }
}

data "kubectl_path_documents" "webshop_routing" {
  pattern = "${path.module}/manifests/webshop_routing.yaml"
  vars = {
    "getarive_dot_com" = var.getarive_dot_com
    "arive_dot_global" = var.arive_dot_global
  }
}

data "kubectl_path_documents" "traefik_hpa" {
  pattern = "${path.module}/manifests/traefik-hpa.yaml"
  vars = {
    "domain" = var.otp_domain
  }
}

resource "kubectl_manifest" "traefik_hpa" {
  count     = 1
  yaml_body = element(data.kubectl_path_documents.traefik_hpa.documents, count.index)
}

resource "kubectl_manifest" "otp_routes" {
  depends_on = [helm_release.ingress_gateway]
  count      = 2
  yaml_body  = element(data.kubectl_path_documents.otp_routes.documents, count.index)
}


resource "kubectl_manifest" "webshop_routing" {
  depends_on = [helm_release.ingress_gateway]
  count      = var.apply_webshop_routing ? 5 : 0
  yaml_body  = element(data.kubectl_path_documents.webshop_routing.documents, count.index)
}

resource "kubectl_manifest" "ingress_tls_store_manifest" {
  depends_on = [helm_release.ingress_gateway]
  count      = 1
  yaml_body  = element(data.kubectl_path_documents.ingress_tls_store.documents, count.index)
}

resource "kubectl_manifest" "ingress_middlewares_manifest" {
  depends_on = [kubernetes_secret.ingress_dashboard_user]
  count      = 2
  yaml_body  = element(data.kubectl_path_documents.ingress_middlewares.documents, count.index)
}

resource "kubectl_manifest" "ingress_routes_manifest" {
  depends_on = [kubectl_manifest.ingress_middlewares_manifest]
  count      = 2
  yaml_body  = element(data.kubectl_path_documents.ingress_routes.documents, count.index)
}
