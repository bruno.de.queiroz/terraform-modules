variable "domain" {
  type        = string
  description = "Default domain."
}

variable "ingress_gateway_chart_version" {
  type        = string
  description = "Ingress Gateway Helm repository version."
  default     = "10.3.2"
}

variable "ingress_gateway_namespace" {
  type        = string
  description = "Ingress Gateway namespace."
  default     = "traefik"
}

variable "ingress_gateway_dashboard_credentials" {
  type        = string
  description = "Ingress Gateway dashboard base64 credentials token."
}

variable "ingress_gateway_wildcard_secret_name" {
  type        = string
  description = "Ingress Gateway wildcard certificate secret name."
  default     = "wildcard-secret"
}

variable "eck_namespace" {
  type        = string
  description = "Elastic cloud namespace."
  default     = "elastic-system"
}

variable "eck_kibana" {
  type        = string
  description = "Elastic cloud kibana service name."
  default     = "kibana-kb-http"
}

variable "otp_domain" {
  type = string
}

variable "apm_secret_token" {
  type = string
}

variable "apm_server_url" {
  type = string
}

variable "apm_env" {
  type = string
}


variable "erp_key" {
  type = string
}

variable "erp_url" {
  type    = string
  default = ""
}

variable "erp_namespace" {
  type = string
}

variable "erp_service" {
  type = string
}

variable "getarive_dot_com" {
  type    = string
  default = ""
}

variable "arive_dot_global" {
  type    = string
  default = ""
}

variable "apply_webshop_routing" {
  type    = bool
  default = false
}

variable "blocked_x_uid_headers" {
  description = "Comma separated list of x-uid headers to block (rate limit 1 per day)"
  type        = string
  default     = ""
}

variable "traefik_requests_cpu" {
  description = "request cpu for traefik"
  type        = string
  default     = "100m"
}

variable "traefik_requests_memory" {
  description = "request memory for traefik"
  type        = string
  default     = "256Mi"
}
