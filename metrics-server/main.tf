terraform {
  required_providers {
    kubernetes = {
      source  = "hashicorp/kubernetes"
      version = ">= 2.0.0"
    }
    kubectl = {
      source  = "gavinbunney/kubectl"
      version = "1.14.0"
    }
    helm = {
      source  = "hashicorp/helm"
      version = ">= 2.3.0"
    }
  }
}

data "kubectl_path_documents" "metrics_server" {
  pattern = "${path.module}/manifests/resources.yaml"
}

resource "kubectl_manifest" "metrics_server" {
  count            = 10
  yaml_body        = element(data.kubectl_path_documents.metrics_server.documents, count.index)
  wait_for_rollout = false
}
