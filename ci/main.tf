terraform {
  required_providers {
    kubernetes = {
      source  = "hashicorp/kubernetes"
      version = ">= 2.0.0"
    }
    kubectl = {
      source  = "gavinbunney/kubectl"
      version = ">= 1.11.3"
    }
    helm = {
      source  = "hashicorp/helm"
      version = ">= 2.3.0"
    }
    gitlab = {
      source  = "gitlabhq/gitlab"
      version = ">= 3.7.0"
    }
    random = {
      source  = "hashicorp/random"
      version = ">= 3.1.0"
    }
  }
}

resource "kubernetes_namespace" "gitlab_manager_namespace" {
  metadata {
    name = "gitlab-managed-apps"
  }
}

resource "kubernetes_service_account" "gitlab_manager_service_account" {
  metadata {
    name      = "gitlab-manager"
    namespace = kubernetes_namespace.gitlab_manager_namespace.metadata[0].name
  }
}

resource "kubernetes_cluster_role_binding" "gitlab_manager_role" {
  metadata {
    name = "gitlab-manager-role"
  }

  role_ref {
    api_group = "rbac.authorization.k8s.io"
    kind      = "ClusterRole"
    name      = "cluster-admin"
  }

  subject {
    kind      = "ServiceAccount"
    name      = kubernetes_service_account.gitlab_manager_service_account.metadata[0].name
    namespace = kubernetes_namespace.gitlab_manager_namespace.metadata[0].name
  }
}

data "kubernetes_secret" "gitlab_manager_secret" {
  metadata {
    name      = kubernetes_service_account.gitlab_manager_service_account.default_secret_name
    namespace = kubernetes_namespace.gitlab_manager_namespace.metadata[0].name
  }
}

resource "gitlab_group_cluster" "gitlab_cluster" {
  depends_on                    = [data.kubernetes_secret.gitlab_manager_secret]
  group                         = var.group_id
  name                          = var.cluster_name
  domain                        = var.cluster_domain
  enabled                       = true
  kubernetes_api_url            = var.cluster_endpoint
  kubernetes_token              = lookup(data.kubernetes_secret.gitlab_manager_secret.data, "token", "")
  kubernetes_ca_cert            = var.cluster_ca_certificate
  kubernetes_authorization_type = "rbac"
  environment_scope             = var.cluster_environment_scope
  management_project_id         = var.project_id
}

resource "gitlab_deploy_token" "deploy_key" {
  group    = var.group_id
  name     = "kubernetes-deploy-token"
  username = "kubernetes-deploy-token"

  scopes = ["read_repository", "read_registry"]
}

resource "kubernetes_secret" "docker" {
  metadata {
    name      = "gitlab-registry"
    namespace = "default"
  }

  type = "kubernetes.io/dockerconfigjson"

  data = {
    ".dockerconfigjson" = jsonencode({
      auths = {
        "https://registry.gitlab.com" : {
          "username" : "${gitlab_deploy_token.deploy_key.name}",
          "password" : "${gitlab_deploy_token.deploy_key.token}",
          "auth" : base64encode("${gitlab_deploy_token.deploy_key.name}:${gitlab_deploy_token.deploy_key.token}")
        }
      }
    })
  }
}

resource "kubernetes_secret" "gitlab_runner_registration_secret" {
  count = var.gitlab_runners_enabled ? 1 : 0
  metadata {
    name      = "gitlab-runner-secret"
    namespace = kubernetes_namespace.gitlab_manager_namespace.metadata[0].name
  }

  data = {
    runner-registration-token = var.group_runners_token
    runner-token              = ""
  }
}

resource "helm_release" "gitlab_runners_release" {
  count      = var.gitlab_runners_enabled ? 1 : 0
  name       = "runner"
  repository = "https://charts.gitlab.io"
  chart      = "gitlab-runner"
  version    = "0.31.0"
  namespace  = kubernetes_namespace.gitlab_manager_namespace.metadata[0].name

  values = [
    file("${path.module}/values.yaml")
  ]
}