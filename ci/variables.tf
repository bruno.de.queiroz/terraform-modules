variable "group_id" {
  description = "Gitlab group id"
  type        = string
}

variable "project_id" {
  description = "Gitlab project id"
  type        = string
}

variable "gitlab_runners_enabled" {
  description = "Create gitlab runners"
  type        = bool
  default     = true
}

variable "group_runners_token" {
  description = "Cluster runners token"
  type        = string
  default     = ""
}

variable "cluster_name" {
  description = "Cluster name"
  type        = string
}

variable "cluster_endpoint" {
  description = "Cluster endpoint"
  type        = string
}

variable "cluster_ca_certificate" {
  description = "Cluster certificates"
  type        = string
}

variable "cluster_domain" {
  description = "Cluster domain"
  type        = string
}

variable "cluster_environment_scope" {
  description = "Cluster environment scope"
  type        = string
}