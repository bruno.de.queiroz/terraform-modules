terraform {
  required_providers {
    google = {
      source  = "hashicorp/google"
      version = ">= 3.87.0"
    }
  }
}

data "google_project" "datalake" {
  project_id = var.datalake_project_id
}

resource "google_storage_bucket" "ingest" {
  name                        = "${data.google_project.datalake.project_id}-${var.environment}-ingest"
  project                     = data.google_project.datalake.project_id
  location                    = var.bucket_location
  force_destroy               = true
  uniform_bucket_level_access = true
  storage_class               = "STANDARD"
}

resource "google_storage_bucket_iam_member" "ingest-admin" {
  bucket = google_storage_bucket.ingest.name
  role   = "roles/storage.admin"
  member = "serviceAccount:${google_service_account.ingest_user.email}"
}

resource "google_storage_bucket_iam_member" "bigquery-bucket-reader" {
  bucket = google_storage_bucket.ingest.name
  role   = "roles/storage.legacyBucketReader"
  member = "serviceAccount:${google_service_account.bigquery_user.email}"
}

resource "google_storage_bucket_iam_member" "bigquery-object-reader" {
  bucket = google_storage_bucket.ingest.name
  role   = "roles/storage.legacyObjectReader"
  member = "serviceAccount:${google_service_account.bigquery_user.email}"
}

resource "google_storage_bucket_iam_member" "project-owner-object-reader" {
  bucket = google_storage_bucket.ingest.name
  role   = "roles/storage.legacyObjectReader"
  member = "projectOwner:${data.google_project.datalake.project_id}"
}

resource "google_service_account" "ingest_user" {
  account_id   = "ingest-user"
  project      = data.google_project.datalake.project_id
  display_name = "ingest user"
  description  = "Service account for data ingestion"
}

resource "google_project_iam_binding" "ingest_user" {
  project = data.google_project.datalake.project_id
  role    = "roles/storage.admin"
  members = [
    "serviceAccount:${google_service_account.ingest_user.email}"
  ]
}

#
# Amplitude user service account and bucket polices
#
resource "google_service_account" "amplitude_user" {
  account_id   = "amplitude-user"
  project      = data.google_project.datalake.project_id
  display_name = "amplitude user"
  description  = "Service account for data ingestion from Amplitude"
}

resource "google_project_iam_binding" "amplitude_user" {
  project = data.google_project.datalake.project_id
  role    = "roles/storage.admin"
  members = [
    "serviceAccount:${google_service_account.amplitude_user.email}"
  ]
}

resource "google_storage_bucket" "amplitude" {
  name                        = "${data.google_project.datalake.project_id}-${var.environment}-amplitude"
  project                     = data.google_project.datalake.project_id
  location                    = var.bucket_location
  force_destroy               = true
  uniform_bucket_level_access = true
  storage_class               = "STANDARD"
}

resource "google_storage_bucket_iam_member" "amplitude-admin" {
  bucket = google_storage_bucket.amplitude.name
  role   = "roles/storage.admin"
  member = "serviceAccount:${google_service_account.amplitude_user.email}"
}

resource "google_storage_bucket_iam_member" "bigquery-bucket-reader-amplitude" {
  bucket = google_storage_bucket.amplitude.name
  role   = "roles/storage.legacyBucketReader"
  member = "serviceAccount:${google_service_account.bigquery_user.email}"
}

resource "google_storage_bucket_iam_member" "bigquery-object-reader-amplitude" {
  bucket = google_storage_bucket.amplitude.name
  role   = "roles/storage.legacyObjectReader"
  member = "serviceAccount:${google_service_account.bigquery_user.email}"
}

resource "google_storage_bucket_iam_member" "project-owner-object-reader-amplitude" {
  bucket = google_storage_bucket.amplitude.name
  role   = "roles/storage.legacyObjectReader"
  member = "projectOwner:${data.google_project.datalake.project_id}"
}

#
# BigQuery reader service account for external tables
#
resource "google_service_account" "bigquery_user" {
  account_id   = "bigquery-user"
  project      = data.google_project.datalake.project_id
  display_name = "bigquery user"
  description  = "Service account to read the data through bigquery"
}

resource "google_project_iam_member" "bigquery_jobs_user" {
  project = data.google_project.datalake.project_id
  role    = "roles/bigquery.jobUser"
  member  = "serviceAccount:${google_service_account.bigquery_user.email}"
}
resource "google_project_iam_member" "bigquery_data_viewer" {
  project = data.google_project.datalake.project_id
  role    = "roles/bigquery.dataViewer"
  member  = "serviceAccount:${google_service_account.bigquery_user.email}"
}
