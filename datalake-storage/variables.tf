variable "bucket_location" {
  description = "Bucket region ID"
  type        = string
  default     = "EUROPE-WEST3"
}

variable "datalake_project_id" {
  description = "Datalake project ID"
  type        = string
}

variable "environment" {
  description = "Environment"
  type        = string
  default     = "staging"
}
