variable "es_node_count" {
  description = "Elasticsearch node count"
  type        = string
  default     = "1"
}

variable "es_volume_size" {
  description = "Elasticsearch volume size"
  type        = string
  default     = "1Gi"
}

variable "es_requests_mem" {
  description = "Elasticsearch request memory"
  type        = string
  default     = "2048"
}

variable "es_requests_cpu" {
  description = "Elasticsearch request cpu"
  type        = string
  default     = "1"
}

variable "es_limits_mem" {
  description = "Elasticsearch limits memory"
  type        = string
  default     = "2048"
}

variable "es_limits_cpu" {
  description = "Elasticsearch limits cpu"
  type        = string
  default     = "2"
}

variable "kibana_hostname" {
  description = "Kibana hostname"
  type        = string
}

variable "kibana_requests_mem" {
  description = "Kibana request memory"
  type        = string
  default     = "512"
}

variable "kibana_requests_cpu" {
  description = "Kibana request cpu"
  type        = string
  default     = "1"
}

variable "kibana_limits_mem" {
  description = "Kibana limits memory"
  type        = string
  default     = "1024"
}

variable "kibana_limits_cpu" {
  description = "Kibana limits cpu"
  type        = string
  default     = "0.5"
}

variable "apm_requests_mem" {
  description = "ApmServer request memory"
  type        = string
  default     = "1024"
}

variable "apm_requests_cpu" {
  description = "ApmServer request cpu"
  type        = string
  default     = "1"
}

variable "apm_limits_mem" {
  description = "ApmServer limits memory"
  type        = string
  default     = "2048"
}

variable "apm_limits_cpu" {
  description = "ApmServer limits cpu"
  type        = string
  default     = "2"
}

variable "beat_mem" {
  description = "Beats memory"
  type        = string
  default     = "300"
}

variable "beat_cpu" {
  description = "Beats cpu"
  type        = string
  default     = "0.5"
}

variable "elastic_password" {
  description = "elasticsearch password"
  type        = string
}

variable "filebeat_index_name" {
  type = string
}

variable "heartbeat_index_name" {
  type = string
}

variable "metricbeat_index_name" {
  type = string
}

variable "apm_index_name" {
  type = string
}

variable "heartbeat_env_tag" {
  type = string
}

variable "elastic_host" {
  type = string
}

variable "elastic_username" {
  type = string
}

variable "masked_data" {
  type = string
}

variable "apm_secret_token" {
  type = string
}
