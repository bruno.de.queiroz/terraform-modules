terraform {
  required_providers {
    kubernetes = {
      source  = "hashicorp/kubernetes"
      version = ">= 2.0.0"
    }
    kubectl = {
      source  = "gavinbunney/kubectl"
      version = "1.14.0"
    }
    helm = {
      source  = "hashicorp/helm"
      version = ">= 2.3.0"
    }
  }
}

data "kubectl_file_documents" "crds" {
  content = file("${path.module}/manifests/crds.yaml")
}

data "kubectl_file_documents" "operator" {
  content = file("${path.module}/manifests/operator.yaml")
}


data "kubectl_path_documents" "apmserver" {
  pattern = "${path.module}/manifests/apmserver.yaml"
  vars = {
    "apm_requests_mem" = var.apm_requests_mem
    "apm_requests_cpu" = var.apm_requests_cpu
    "apm_limits_mem"   = var.apm_limits_mem
    "apm_limits_cpu"   = var.apm_limits_cpu
    "elastic_password" = var.elastic_password
    "apm_index_name"   = var.apm_index_name
    "elastic_host"     = var.elastic_host
    "elastic_username" = var.elastic_username
    "apm_secret_token" = var.apm_secret_token
  }
}

data "kubectl_path_documents" "filebeat" {
  pattern = var.masked_data ? "${path.module}/manifests/filebeat-with-data-masking.yaml" : "${path.module}/manifests/filebeat.yaml"
  vars = {
    "beat_requests_mem"   = var.beat_mem
    "beat_requests_cpu"   = var.beat_cpu
    "beat_limits_mem"     = var.beat_mem
    "beat_limits_cpu"     = var.beat_cpu
    "elastic_password"    = var.elastic_password
    "filebeat_index_name" = var.filebeat_index_name
    "elastic_host"        = var.elastic_host
    "elastic_username"    = var.elastic_username
  }
}

data "kubectl_path_documents" "metricbeat" {
  pattern = "${path.module}/manifests/metricbeat.yaml"
  vars = {
    "beat_requests_mem"     = var.beat_mem
    "beat_requests_cpu"     = var.beat_cpu
    "beat_limits_mem"       = var.beat_mem
    "beat_limits_cpu"       = var.beat_cpu
    "elastic_password"      = var.elastic_password
    "metricbeat_index_name" = var.metricbeat_index_name
    "elastic_host"          = var.elastic_host
    "elastic_username"      = var.elastic_username
  }
}

data "kubectl_path_documents" "heartbeat" {
  pattern = "${path.module}/manifests/heartbeat.yaml"
  vars = {
    "beat_requests_mem"    = var.beat_mem
    "beat_requests_cpu"    = var.beat_cpu
    "beat_limits_mem"      = var.beat_mem
    "beat_limits_cpu"      = var.beat_cpu
    "elastic_password"     = var.elastic_password
    "heartbeat_index_name" = var.heartbeat_index_name
    "heartbeat_env_tag"    = var.heartbeat_env_tag
    "elastic_host"         = var.elastic_host
    "elastic_username"     = var.elastic_username
  }
}

resource "kubectl_manifest" "crds" {
  count            = 7
  yaml_body        = element(data.kubectl_file_documents.crds.documents, count.index)
  wait_for_rollout = false
}

resource "kubectl_manifest" "operator" {
  depends_on = [kubectl_manifest.crds]
  count      = 11
  yaml_body  = element(data.kubectl_file_documents.operator.documents, count.index)
}

resource "kubectl_manifest" "apmserver" {
  count     = 2
  yaml_body = element(data.kubectl_path_documents.apmserver.documents, count.index)
}

resource "kubectl_manifest" "filebeat" {
  count     = 4
  yaml_body = element(data.kubectl_path_documents.filebeat.documents, count.index)
}

resource "kubectl_manifest" "metricbeat" {
  count     = 4
  yaml_body = element(data.kubectl_path_documents.metricbeat.documents, count.index)
}

resource "kubectl_manifest" "heartbeat" {
  count     = 8
  yaml_body = element(data.kubectl_path_documents.heartbeat.documents, count.index)
}


