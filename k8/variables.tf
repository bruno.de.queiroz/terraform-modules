variable "cluster_name" {
  description = "Cluster name"
  type        = string
}

variable "cluster_region" {
  description = "Region ID"
  type        = string
  default     = "fra1"
}

variable "cluster_size" {
  description = "Cluster size"
  type        = string
  default     = "s-4vcpu-8gb"
}

variable "cluster_node_count" {
  description = "Cluster nodes count for when cluster_auto_scale_enabled is `false`"
  type        = number
  default     = 2
}


variable "cluster_auto_scale_enabled" {
  description = "To enable the cluster auto scaling"
  type        = bool
  default     = false
}

variable "cluster_min_nodes" {
  description = "Minimum number of nodes"
  type        = number
  default     = 3
}

variable "cluster_max_nodes" {
  description = "Maximum number of nodes"
  type        = number
  default     = 4
}

variable "do_version" {
  description = "Cluster slug version"
  type        = string
  default     = "1.21.3-do.0"
}