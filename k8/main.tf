terraform {
  required_providers {
    digitalocean = {
      source  = "digitalocean/digitalocean"
      version = "~> 2.0"
    }
  }
}

resource "digitalocean_kubernetes_cluster" "k8_cluster" {
  name   = "${var.cluster_name}-${var.cluster_region}"
  region = var.cluster_region
  # Grab the latest version slug from `doctl kubernetes options versions`
  version      = var.do_version
  auto_upgrade = true

  maintenance_policy {
    start_time = "04:00"
    day        = "monday"
  }

  node_pool {
    name       = "default-pool"
    size       = var.cluster_size
    node_count = var.cluster_auto_scale_enabled ? var.cluster_min_nodes : var.cluster_node_count
    auto_scale = var.cluster_auto_scale_enabled
    min_nodes  = var.cluster_min_nodes
    max_nodes  = var.cluster_max_nodes
  }
}