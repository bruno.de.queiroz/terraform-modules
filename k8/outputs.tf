output "cluster_id" {
  sensitive = true
  value     = digitalocean_kubernetes_cluster.k8_cluster.id
}

output "cluster_name" {
  sensitive = true
  value     = digitalocean_kubernetes_cluster.k8_cluster.name
}

output "cluster_endpoint" {
  sensitive = true
  value     = digitalocean_kubernetes_cluster.k8_cluster.endpoint
}

output "cluster_token" {
  sensitive = true
  value     = digitalocean_kubernetes_cluster.k8_cluster.kube_config[0].token
}

output "cluster_ca_certificate" {
  sensitive = true
  value = base64decode(
    digitalocean_kubernetes_cluster.k8_cluster.kube_config[0].cluster_ca_certificate
  )
}